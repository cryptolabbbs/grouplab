# Групповая лабораторная работа
## Задание 7E7-5

Программа содержит вспомогательные функции которые использовались при анализе шифра. Если запустить программу, она выведет таблицу кодировок и расшифрованный текст, используя эти функции  

Чтобы запустить программу понадобится Python 3.11.5

```python
python group.py
```
